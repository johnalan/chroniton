sublime-chroniton
================

Fully automatic time tracking for Sublime Text 2 & 3.

Installation
------------

Heads Up! For Sublime Text 2 on Windows & Linux, Chroniton depends on [Python](http://www.python.org/getit/) being installed to work correctly.
    
1. Install [Package Control](https://packagecontrol.io/installation).

2. Using [Package Control](https://packagecontrol.io/docs/usage):

  a) Inside Sublime, press `ctrl+shift+p`(Windows, Linux) or `cmd+shift+p`(OS X).

  b) Type `install`, then press `enter` with `Package Control: Install Package` selected.

  c) Type `chroniton`, then press `enter` with the `Chroniton` plugin selected.

3. Enter your [api key](https://chroniton.com/settings#apikey), then press `enter`.

4. Use Sublime and your time will be tracked for you automatically.

5. Visit https://chroniton.com/dashboard to see your logged time.

Screen Shots
------------

![Project Overview](https://chroniton.com/static/img/ScreenShots/ScreenShot-2014-10-29.png)

