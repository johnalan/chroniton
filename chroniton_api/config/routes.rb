Rails.application.routes.draw do
  scope '/api/v1' do
    resources :activity, :except => [:new, :edit, :update]
  end
end
